ADMINISTRACION - R1
access-list 101 permit icmp host 192.168.10.2 host 192.168.10.3
access-list 101 permit icmp host 192.168.10.2 host 192.168.10.18
access-list 101 permit icmp host 192.168.10.2 host 192.168.10.34
access-list 101 permit icmp host 192.168.10.2 host 192.168.10.35
access-list 101 permit icmp host 192.168.10.3 host 192.168.10.2
access-list 101 permit icmp host 192.168.10.3 host 192.168.10.18
access-list 101 permit icmp host 192.168.10.3 host 192.168.10.34
access-list 101 permit icmp host 192.168.10.3 host 192.168.10.35
access-list 101 permit icmp 192.168.10.4 0.0.0.12 192.168.10.16 0.0.0.15
access-list 101 permit icmp 192.168.10.4 0.0.0.12 192.168.10.32 0.0.0.15
access-list 101 permit icmp 192.168.10.0 0.0.0.15 host 192.168.10.98
access-list 101 permit icmp 192.168.10.4 0.0.0.12 host 192.168.10.106
access-list 101 deny ip any any


----------------------------------------------------------------------------------
INGENIERIA - R1
access-list 102 permit icmp 192.168.10.48 0.0.0.15 host 192.168.10.106
access-list 102 permit icmp 192.168.10.48 0.0.0.15 192.168.10.64 0.0.0.15
access-list 102 permit icmp 192.168.10.48 0.0.0.15 192.168.10.80 0.0.0.15
access-list 102 deny ip any any


-----------------------------------------------------------------------------------

ADMINISTRACION - R2
access-list 103 permit icmp host 192.168.10.18 host 192.168.10.2
access-list 103 permit icmp host 192.168.10.18 host 192.168.10.3
access-list 103 permit icmp host 192.168.10.18 host 192.168.10.34
access-list 103 permit icmp host 192.168.10.18 host 192.168.10.35
access-list 103 deny icmp host 192.168.10.18 host 192.168.10.19
access-list 103 permit icmp 192.168.10.19 0.0.0.12 192.168.10.0 0.0.0.15
access-list 103 permit icmp 192.168.10.19 0.0.0.12 192.168.10.32 0.0.0.15
access-list 103 permit icmp 192.168.10.16 0.0.0.15 host 192.168.10.98
access-list 103 permit icmp 192.168.10.19 0.0.0.12 host 192.168.10.106
access-list 103 deny ip any any

--------------------------------------------------------------------------------------
INGENIERIA - R2
access-list 104 permit icmp 192.168.10.64 0.0.0.15 192.168.10.48 0.0.0.15
access-list 104 permit icmp 192.168.10.64 0.0.0.15 192.168.10.80 0.0.0.15
access-list 104 permit icmp 192.168.10.64 0.0.0.15 host 192.168.10.106
access-list 104 deny ip any any

--------------------------------------------------------------------------------------
ADMINISTRACION - R3 

access-list 105 permit icmp host 192.168.10.34 host 192.168.10.35
access-list 105 permit icmp host 192.168.10.34 host 192.168.10.2
access-list 105 permit icmp host 192.168.10.34 host 192.168.10.3
access-list 105 permit icmp host 192.168.10.34 host 192.168.10.18
access-list 105 permit icmp host 192.168.10.35 host 192.168.10.34
access-list 105 permit icmp host 192.168.10.35 host 192.168.10.2
access-list 105 permit icmp host 192.168.10.35 host 192.168.10.3
access-list 105 permit icmp host 192.168.10.35 host 192.168.10.18
access-list 105 permit icmp 192.168.10.36 0.0.0.12 192.168.10.0 0.0.0.15
access-list 105 permit icmp 192.168.10.36 0.0.0.12 192.168.10.16 0.0.0.15
access-list 105 permit icmp 192.168.10.32 0.0.0.15 host 192.168.10.98
access-list 105 permit icmp 192.168.10.36 0.0.0.12 host 192.168.10.106
access-list 105 deny ip any any

------------------------------------------------------------------------------------------
INGENIERIA - R3
access-list 106 permit icmp 192.168.10.80 0.0.0.15 192.168.10.48 0.0.0.15
access-list 106 permit icmp 192.168.10.80 0.0.0.15 192.168.10.64 0.0.0.15
access-list 106 permit icmp 192.168.10.80 0.0.0.15 host 192.168.10.106
access-list 106 deny ip any any
